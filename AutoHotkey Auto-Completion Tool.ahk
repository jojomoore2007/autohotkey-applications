﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

::edit_autocomplete::
BlockInput On
Send Ready to add auto-completions.
Sleep 3000
begin:
Send ^a{BS}
Send Type the string that will get replaced{,} then hit Enter:`n
hotstr := "::"
Loop
{
	BlockInput Off
	Input key, L1, {Enter}{BS}:
	ErrL := ErrorLevel
	KeyWait %key%
	KeyWait SubStr(ErrorLevel, 8)
	BlockInput On
	if not ((ErrL = "EndKey:Enter") or (ErrL = "EndKey:Backspace") or (ErrL = "EndKey::")) {
		hotstr := hotstr . key
		Send %key%
	} else if (ErrL = "EndKey:Backspace") {
		hotstr := SubStr(hotstr, 1, -1)
		Send {BS}
	} else if (ErrL = "EndKey::") {
		expanded := expanded . "{:}"
	}
} Until (ErrL = "EndKey:Enter")
Send ^a{BS}
Send Now type the string that will replace it{,} then hit Enter again:`n
expanded := ""
Loop
{
	BlockInput Off
	Input key, L1, {Enter}{BS}:
	ErrL := ErrorLevel
	KeyWait %key%
	KeyWait SubStr(ErrorLevel, 8)
	BlockInput On
	if not ((ErrL = "EndKey:Enter") or (ErrL = "EndKey:Backspace") or (ErrL = "EndKey::")) {
		expanded := expanded . key
		Send %key%
	} else if (ErrL = "EndKey:Backspace") {
		expanded := SubStr(expanded, 1, -1)
		Send {BS}
	} else if (ErrL = "EndKey::") {
		expanded := expanded . "{:}"
	}
} Until (ErrL = "EndKey:Enter")
Send ^a{BS}
finalOutput := "::" . hotstr . "::" . expanded
Send The hotstring to replace is:`n%hotstr%`nThe result will be:`n%expanded%`n`nIs this correct{?} (Y/N)`n
Loop
{
	BlockInput Off
	Input YN, L1
	KeyWait %YN%
	BlockInput On
	if not ((YN = "Y") or (YN = "N")) {
		Send Come on{,} only Y or N{!}
		Sleep 3000
		Send {BS 21}
	} else if (YN = "Y") {
		Send ^a{BS}
		goto save
	} else {
		goto begin
	}
}
save:
Send Saving... `nDon't turn off your computer...
FileAppend `n%finalOutput%, %A_ScriptFullPath%
Sleep 3000
Send ^a{BS}
Send Done saving. Would you like to add another auto-completion{?}(Y/N)`n
Loop
{
	BlockInput Off
	Input YN, L1
	KeyWait %YN%
	BlockInput On
	if not ((YN = "Y") or (YN = "N")) {
		Send Come on{,} only Y or N{!}
		Sleep 3000
		Send {BS 21}
	} else if (YN = "Y") {
		goto begin
	} else {
		Send ^a{BS}
		goto reloadScript
	}
}
reloadScript:
Reload
Sleep 1000
return

;Anything below is a user creation. The only hotstring that was added by me was the one for setting up hotstrings. Other than that, I've done nothing with hotstrings, so if you see something you didn't add, just contact me at jojo62815@gmail.com, and in the subject line, put "AHK Auto-Completion Bug" so it'll show up as an important message and float to the top of all my other emails.
