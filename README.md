# README #

This is a repository for all the AutoHotkey apps and games I've made in AHK.
Pretty self-explanatory.

If you find any bugs, report them in the [Issues](https://bitbucket.org/jojomoore2007/autohotkey-applications/issues?status=new&status=open).