﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

::remapKBD::
BlockInput On
remapFrom:
KeyCombo := ""
KeyComboUserView := ""
Send Now remapping.
Sleep 2000
Send {BS 14}Press the key combo to remap.
BlockInput Off
Input Key, L1
BlockInput On
StringLower Key, Key
if GetKeyState("LControl") {
	KeyCombo := KeyCombo . "<^"
	KeyComboUserView := KeyComboUserView . "Left Control {+} "
	Send {LControl up}
}
if GetKeyState("RControl") {
	KeyCombo := KeyCombo . ">^"
	KeyComboUserView := KeyComboUserView . "Right Control {+} "
	Send {RControl up}
}
if GetKeyState("LAlt") {
	KeyCombo := KeyCombo . "<!"
	KeyComboUserView := KeyComboUserView . "Left Alt {+} "
	Send {LAlt up}
}
if GetKeyState("RAlt") {
	KeyCombo := KeyCombo . ">!"
	KeyComboUserView := KeyComboUserView . "Right Alt {+} "
	Send {RAlt up}
}
if GetKeyState("LShift") {
	KeyCombo := KeyCombo . "+"
	KeyComboUserView := KeyComboUserView . "Left Shift {+} "
	Send {LShift up}
}
if GetKeyState("RShift") {
	KeyCombo := KeyCombo . "+"
	KeyComboUserView := KeyComboUserView . "Right Shift {+} "
	Send {RShift up}
}
if GetKeyState("LWin") {
	KeyCombo := KeyCombo . "<#"
	KeyComboUserView := KeyComboUserView . "Left Windows {+} "
	Send {LWin up}
}
if GetKeyState("RWin") {
	KeyCombo := KeyCombo . ">#"
	KeyComboUserView := KeyComboUserView . "Right Windows {+} "
	Send {RWin up}
}
KeyCombo := KeyCombo . Key
Sleep 500
BlockInput Off
BlockInput On
StringUpper CapitalKey, Key
KeyComboUserView := KeyComboUserView . CapitalKey
Send `n
Send You pressed:`n
Send %KeyComboUserView%
Send `n
Send Is this correct?
Send `n
Loop
{
	BlockInput Off
	Input YN, L1
	BlockInput On
	if not ((YN = "Y") or (YN = "N")) {
		Send Come on{,} only Y or N{!}
		Sleep 3000
		Send {BS 21}
	} else if (YN = "Y") {
		goto remapTo
	} else {
		Send ^a
		Send {BS}
		goto remapFrom
	}
}
remapTo:
Send ^a
Send {BS}
finalRemapCombo := KeyCombo . "::"
KeyCombo := ""
KeyComboUserView := ""
Send Press the key combo to remap to.
BlockInput Off
Input Key, L1
StringLower Key, Key
if GetKeyState("LControl") {
	KeyCombo := KeyCombo . "<^"
	KeyComboUserView := KeyComboUserView . "Left Control {+} "
	Send {LControl up}
}
if GetKeyState("RControl") {
	KeyCombo := KeyCombo . ">^"
	KeyComboUserView := KeyComboUserView . "Right Control {+} "
	Send {RControl up}
}
if GetKeyState("LAlt") {
	KeyCombo := KeyCombo . "<!"
	KeyComboUserView := KeyComboUserView . "Left Alt {+} "
	Send {LAlt up}
}
if GetKeyState("RAlt") {
	KeyCombo := KeyCombo . ">!"
	KeyComboUserView := KeyComboUserView . "Right Alt {+} "
	Send {RAlt up}
}
if GetKeyState("LShift") {
	KeyCombo := KeyCombo . "+"
	KeyComboUserView := KeyComboUserView . "Left Shift {+} "
	Send {LShift up}
}
if GetKeyState("RShift") {
	KeyCombo := KeyCombo . "+"
	KeyComboUserView := KeyComboUserView . "Right Shift {+} "
	Send {RShift up}
}
if GetKeyState("LWin") {
	KeyCombo := KeyCombo . "<#"
	KeyComboUserView := KeyComboUserView . "Left Windows {+} "
	Send {LWin up}
}
if GetKeyState("RWin") {
	KeyCombo := KeyCombo . ">#"
	KeyComboUserView := KeyComboUserView . "Right Windows {+} "
	Send {RWin up}
}
KeyCombo := KeyCombo . Key
Sleep 2000
BlockInput On
StringUpper CapitalKey, Key
KeyComboUserView := KeyComboUserView . CapitalKey
Send `n
Send You pressed:`n
Send %KeyComboUserView%
Send `n
Send Is this correct?
Send `n
Loop
{
	BlockInput Off
	Input YN, L1
	BlockInput On
	if not ((YN = "Y") or (YN = "N")) {
		Send Come on{,} only Y or N{!}
		Sleep 3000
		Send {BS 21}
	} else if (YN = "Y") {
		goto remapSave
	} else {
		Send ^a
		Send {BS}
		goto remapTo
	}
}
remapSave:
Send ^a
Send {BS}
finalRemapCombo := finalRemapCombo . KeyCombo
Send Saving...
FileAppend `n%finalRemapCombo%, %A_ScriptFullPath%
Sleep 1000
Send {BS 9}
Send Done saving.
Sleep 3000
Send ^a
Send {BS}
Send Would you like to add another remapping{?}
Loop
{
	BlockInput Off
	Input YN, L1
	BlockInput On
	if not ((YN = "Y") or (YN = "N")) {
		Send Come on{,} only Y or N{!}
		Sleep 3000
		Send {BS 21}
	} else if (YN = "Y") {
		Send ^a
		Send {BS}
		goto remapFrom
	} else {
		Send ^a
		Send {BS}
		goto remapEnd
	}
}
remapEnd:
Send Goodbye{!}
Sleep 1000
Send ^a
Send {BS}
Reload
Sleep 1000
return

; Anything from here on out is a remapping added by the user. I didn't add anything here, so the keyboard should behave normally until remapped. If it doesn't, just contact me at jojo62815@gmail.com, and in the subject line, put "AHK Remapping Bug" so it'll show up as an important message and float to the top of all my other emails.
